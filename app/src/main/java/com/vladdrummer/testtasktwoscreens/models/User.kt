package com.vladdrummer.testtasktwoscreens.models

data class User(
    val id: Int,
    val name: String
)