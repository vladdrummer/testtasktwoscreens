package com.vladdrummer.testtasktwoscreens.models

data class Post (
    val userId: Int,
    val title: String,
    val body: String
)