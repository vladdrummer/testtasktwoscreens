package com.vladdrummer.testtasktwoscreens.data

import com.vladdrummer.testtasktwoscreens.models.Post
import com.vladdrummer.testtasktwoscreens.models.User
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface Api {

    @GET("posts")
    fun getPosts() : Observable<List<Post>>

    @POST("posts")
    fun setPost(@Body post: Post) : Observable<Post>

    @GET("users/{id}")
    fun getUserById(@Path("id") id: Int): Observable<User>
}