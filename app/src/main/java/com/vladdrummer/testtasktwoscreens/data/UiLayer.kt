package com.vladdrummer.testtasktwoscreens.data

import com.vladdrummer.testtasktwoscreens.ui.main.MainPresenter

object UiLayer {
    lateinit var mainPresenter: MainPresenter
    fun openMainScreen(){
        mainPresenter.openPostsFragment()
    }

    fun openMakePostScreen(){
        mainPresenter.navigateToNewPostFragment()
    }

    fun showError(t: Throwable){
        mainPresenter.showError(t)
    }

    fun showMessage(message: String){
        mainPresenter.showMessage(message)
    }

    fun goBack(){
        mainPresenter.goBack()
    }
}