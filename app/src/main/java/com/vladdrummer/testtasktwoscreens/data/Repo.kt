package com.vladdrummer.testtasktwoscreens.data

import com.vladdrummer.testtasktwoscreens.models.Post
import com.vladdrummer.testtasktwoscreens.models.User
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

object Repo : Api{

    val compositeDisposable = CompositeDisposable()
    var posts = mutableListOf<Post>()
    var users = mutableListOf<User>()

    private val api =
        RetrofitServiceGenerator.createService(
            Api::class.java,
            "${Setup.URL}"
        )
    override fun getPosts(): Observable<List<Post>> {
       return api.getPosts()
    }

    override fun setPost(post: Post): Observable<Post> {
        return api.setPost(post)
    }

    fun onDestroy(){
        compositeDisposable.dispose()
    }

    override fun getUserById(id: Int): Observable<User> {
        return api.getUserById(id)
    }
}