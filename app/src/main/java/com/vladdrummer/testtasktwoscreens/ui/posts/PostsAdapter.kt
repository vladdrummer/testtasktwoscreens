package com.vladdrummer.testtasktwoscreens.ui.posts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vladdrummer.testtasktwoscreens.R
import com.vladdrummer.testtasktwoscreens.data.Repo
import com.vladdrummer.testtasktwoscreens.data.UiLayer
import com.vladdrummer.testtasktwoscreens.models.Post
import kotlinx.android.synthetic.main.row_post.view.*

class PostsAdapter (private var posts: List<Post>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_post, parent, false))
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val movieViewHolder = viewHolder as MovieListViewHolder
        movieViewHolder.bindView(posts[position])
    }

    class MovieListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(post: Post) {
            itemView.tvBody.text = post.body
            itemView.tvTitle.text = post.title
            //First, find the User in cache
            val cachedUser = Repo.users.firstOrNull { it.id == post.userId }
            //If it doesn't exist - take it from api
            if (cachedUser == null) {
                Repo.compositeDisposable.add(
                    Repo.getUserById(post.userId).subscribe(
                        { user ->
                            if (user != null) {
                                val cachedUser = Repo.users.firstOrNull { it.id == user.id }
                                if (cachedUser == null) {
                                    Repo.users.add(user)
                                }
                                itemView.tvName.text = user.name
                            } else {
                                UiLayer.showError(Throwable(itemView.context.getString(R.string.bad_user_data)))
                            }
                        },
                        {
                            UiLayer.showError(it)
                        }
                    )
                )
            } else {
                //If it exist - get it from cache
                itemView.tvName.text = cachedUser.name
            }
        }
    }
}