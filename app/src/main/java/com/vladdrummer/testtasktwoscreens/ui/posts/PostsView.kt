package com.vladdrummer.testtasktwoscreens.ui.posts

import com.arellomobile.mvp.MvpView

interface PostsView : MvpView {
    fun showPosts()
}