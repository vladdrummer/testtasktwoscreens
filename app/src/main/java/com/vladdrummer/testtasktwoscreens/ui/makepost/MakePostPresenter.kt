package com.vladdrummer.testtasktwoscreens.ui.makepost

import android.content.Context
import android.content.res.Resources
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.vladdrummer.testtasktwoscreens.R
import com.vladdrummer.testtasktwoscreens.data.Repo
import com.vladdrummer.testtasktwoscreens.data.UiLayer
import com.vladdrummer.testtasktwoscreens.models.Post
import com.vladdrummer.testtasktwoscreens.models.User

@InjectViewState
class MakePostPresenter: MvpPresenter<MakePostView>() {

    var user: User? = null
    lateinit var resources: Resources
    init {
        Repo.users.getOrNull(0)?.let {user->
            this.user = user
        }
    }

    fun init(context: Context){
        resources = context.resources
        if (user == null) {
            user = User(0,"")
        }
        user?.let {user->
            viewState.setName(user.name)
        }
        viewState.enableUI(true)
    }

    fun send(title:String, text: String){
        viewState.enableUI(false)
        if (text.length > 5 && title.length > 5) {
            user?.let {user->
                Repo.compositeDisposable.add(
                    Repo.setPost(
                        Post (
                            user.id,
                            title,
                            text
                        )
                    ).subscribe(
                        {receivedPost->
                            viewState.enableUI(true)
                            UiLayer.showMessage(resources.getString(R.string.success_message, receivedPost.title))
                            Repo.posts.add(receivedPost)
                            UiLayer.goBack()
                        },
                        {
                            UiLayer.showError(it)
                            viewState.enableUI(true)
                        }
                    )
                )
            }
        } else {
            UiLayer.showError(Throwable(resources.getString(R.string.message_too_short)))
            viewState.enableUI(true)
        }
    }
}