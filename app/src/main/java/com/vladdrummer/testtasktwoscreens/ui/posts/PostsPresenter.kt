package com.vladdrummer.testtasktwoscreens.ui.posts

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.vladdrummer.testtasktwoscreens.R
import com.vladdrummer.testtasktwoscreens.data.Repo
import com.vladdrummer.testtasktwoscreens.data.UiLayer
import com.vladdrummer.testtasktwoscreens.models.Post

@InjectViewState
class PostsPresenter : MvpPresenter<PostsView>() {

    var context:Context? = null

    fun init(context: Context?){
        this.context = context
    }

    fun getPosts() : List<Post>{
        return Repo.posts
    }

    fun fillPostsIfEmpty(){
        if (Repo.posts.isEmpty()) {
            Repo.compositeDisposable.add(
                Repo.getPosts().subscribe(
                    {
                        if (it.isNotEmpty()) {
                            Repo.posts.clear()
                            Repo.posts.addAll(it)
                            viewState.showPosts()
                        } else {
                            context?.let { context ->
                                UiLayer.showError(Throwable(context.getString(R.string.no_data_err)))
                            }
                        }
                    },
                    {
                        UiLayer.showError(it)
                    }
                )
            )
        }
    }

    fun notifyPostsUpdate(){
        viewState.showPosts()
    }

    fun openMakePostFragment(){
        UiLayer.openMakePostScreen()
    }
}