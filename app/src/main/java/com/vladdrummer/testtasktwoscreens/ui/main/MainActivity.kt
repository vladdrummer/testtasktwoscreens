package com.vladdrummer.testtasktwoscreens.ui.main

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.vladdrummer.testtasktwoscreens.R
import com.vladdrummer.testtasktwoscreens.ui.makepost.MakePostFragment
import com.vladdrummer.testtasktwoscreens.ui.posts.PostsFragment

class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    private lateinit var snackView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        snackView = findViewById(android.R.id.content)
        if (savedInstanceState == null) {
            mainPresenter.openPostsFragment()
        }
    }

    override fun navigateToMakePostFragment() {
        supportFragmentManager.beginTransaction().add(R.id.container, MakePostFragment()).addToBackStack(MakePostFragment::class.java.name).commitAllowingStateLoss()
    }

    override fun openPostsFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, PostsFragment()).commitAllowingStateLoss()
    }

    override fun showError(t: Throwable) {
        Snackbar.make(snackView, t.localizedMessage, Snackbar.LENGTH_LONG).show()
    }

    override fun showMessage(message: String) {
        Snackbar.make(snackView, message, Snackbar.LENGTH_LONG).show()
    }

    override fun goBack() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
            supportFragmentManager.findFragmentById(R.id.container)?.let {fragment ->
                if (fragment is PostsFragment) {
                    fragment.presenter.notifyPostsUpdate()
                }
            }
        }
    }
}
