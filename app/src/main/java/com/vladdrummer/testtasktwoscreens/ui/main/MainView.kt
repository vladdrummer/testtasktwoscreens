package com.vladdrummer.testtasktwoscreens.ui.main

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface MainView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy ::class)
    fun navigateToMakePostFragment()

    @StateStrategyType(OneExecutionStateStrategy ::class)
    fun openPostsFragment()

    @StateStrategyType(OneExecutionStateStrategy ::class)
    fun showError(t: Throwable)

    @StateStrategyType(OneExecutionStateStrategy ::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy ::class)
    fun goBack()
}