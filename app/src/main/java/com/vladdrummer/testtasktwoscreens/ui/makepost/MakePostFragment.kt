package com.vladdrummer.testtasktwoscreens.ui.makepost

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.vladdrummer.testtasktwoscreens.R
import kotlinx.android.synthetic.main.fragment_makepost.*

class MakePostFragment  : MvpAppCompatFragment(), MakePostView {

    @InjectPresenter
    lateinit var presenter: MakePostPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_makepost, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {context ->
            presenter.init(context)
        }
        btnSend.setOnClickListener {
            presenter.send(etTitle.text.toString(), etBody.text.toString()) }
    }

    override fun enableUI(enable: Boolean) {
        etTitle.isEnabled = enable
        etBody.isEnabled = enable
        btnSend.isEnabled = enable
        flWaitPlz.visibility = if (enable) View.GONE else View.VISIBLE
    }

    override fun setName(name: String) {
        tvName.visibility = if (TextUtils.isEmpty(name)) View.GONE else View.VISIBLE
        tvName.text = name
    }
}