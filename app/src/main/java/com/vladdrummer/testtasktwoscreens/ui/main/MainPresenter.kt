package com.vladdrummer.testtasktwoscreens.ui.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.vladdrummer.testtasktwoscreens.data.Repo
import com.vladdrummer.testtasktwoscreens.data.UiLayer

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {

    init {
        UiLayer.mainPresenter = this
    }

    fun navigateToNewPostFragment(){
        viewState.navigateToMakePostFragment()
    }

    fun openPostsFragment(){
        viewState.openPostsFragment()
    }

    fun showError(t: Throwable){
        viewState.showError(t)
    }

    fun showMessage(message: String){
        viewState.showMessage(message)
    }

    fun goBack(){
        viewState.goBack()
    }

    override fun onDestroy() {
        Repo.onDestroy()
        super.onDestroy()
    }
}