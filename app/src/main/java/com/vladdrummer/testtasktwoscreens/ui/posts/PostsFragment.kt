package com.vladdrummer.testtasktwoscreens.ui.posts

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.vladdrummer.testtasktwoscreens.R
import com.vladdrummer.testtasktwoscreens.data.Const
import com.vladdrummer.testtasktwoscreens.data.Repo
import kotlinx.android.synthetic.main.fragment_posts.*

class PostsFragment : MvpAppCompatFragment(), PostsView {

    @InjectPresenter
    lateinit var presenter: PostsPresenter
    private lateinit var adapter: PostsAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private var recyclerState: Parcelable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        presenter.init(context)
        return inflater.inflate(R.layout.fragment_posts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layoutManager = LinearLayoutManager(context)
        adapter = PostsAdapter(presenter.getPosts())
        rv.layoutManager = layoutManager
        rv.adapter = adapter

        fab.setOnClickListener {
            presenter.openMakePostFragment()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(Const.RECYCLER_VIEW_STATE, layoutManager.onSaveInstanceState())
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.let {
            recyclerState = savedInstanceState.getParcelable(Const.RECYCLER_VIEW_STATE)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.fillPostsIfEmpty()
        recyclerState?.let{parcel ->
            layoutManager.onRestoreInstanceState(parcel)
        }
    }
    override fun showPosts() {
        adapter.notifyDataSetChanged()
    }
}