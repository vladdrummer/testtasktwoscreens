package com.vladdrummer.testtasktwoscreens.ui.makepost

import com.arellomobile.mvp.MvpView

interface MakePostView : MvpView {
    fun enableUI(enable: Boolean)
    fun setName(name: String)
}